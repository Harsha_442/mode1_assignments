package com.carinventory.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Scanner;

import com.carinventory.pojo.CarInventoryPojo;
import com.connection.DBConnection;



public class CarInventoryCrudOperations {
 
   public void addMethod(CarInventoryPojo pojo) throws Exception
   {
	   Connection con=null;
	   Statement st=null;
	   PreparedStatement ps=null;
	   ResultSet rs=null;
	   try
	   {
		    con=DBConnection.getConnection();
		    st=con.createStatement();
		    rs=st.executeQuery("select count(ID) as rowcount from tbl_car"); // To get the count of rows in the table 
			rs.next();
			int count=rs.getInt("rowcount"); //count variable contains the count of rows in the table
			if(count>=20) 
			{
				System.out.println("Unabe to add, because table count is already 20"); 
			}
			else 
			{
			
			ps=con.prepareStatement("insert into tbl_car values(?,?,?,?,?)");  // Inserting values into the table
			ps.setInt(1, pojo.getId());
			ps.setString(2, pojo.getMake());
			ps.setString(3, pojo.getModel());
			ps.setInt(4, pojo.getYear());
			ps.setFloat(5, pojo.getSales_price());
			int i=ps.executeUpdate();  //  Executing the above insert query
			System.out.println(i+"Row inserted");	
			}
	   }
	   catch(InputMismatchException s)
	   {
		   System.out.println("Please enter the valid details");
	   }
	   finally
	   {
		   con.close();
		   ps.close();
		   rs.close();
		   st.close();	   
	   }
   }
	   public void listMethod() throws Exception
	   {
		   Connection con=null;
		   Statement st=null;
		   ResultSet rs=null;
		   try
		   {
			   con=DBConnection.getConnection();
			   st=con.createStatement();
				rs = st.executeQuery("select * from tbl_car");  // To display the data from the table
				if(!rs.next()) 
				{
					System.out.println("There are currently no cars in the catalog");
				}
				else
				{	
				do // To display all the rows data 
				{
				  System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getString(3)+" "+rs.getInt(4)+" "+"$"+rs.getFloat(5));
				}while(rs.next());
					
				rs=st.executeQuery("select count(ID) as rowcount from tbl_car");  // To get the count of cars in the table 
				rs.next(); // To make the header to move next
				int count1=rs.getInt("rowcount");
				System.out.println("No of cars:"+count1); // To display the no of cars
				rs=st.executeQuery("select sum(Sales_price) as totalprice from tbl_car"); //To get the sum of prices of all cars in the tables 
				rs.next(); // To make the header to move next
				float sum=rs.getInt("totalprice");
				System.out.println("Total inventory: "+"$"+sum); // to display the sum of prices of all cars in the table
				}
		   }
		   catch(Exception e)
		   {
			   System.out.println(e);
	       }
		   finally
		   {
			   con.close();
			   rs.close();
			   st.close();	   
		   }
	   }
		public void updateMethod(int id)throws Exception
		{
			   Scanner sc=new Scanner(System.in);
			   Connection con=null;
			   Statement st=null;
			   PreparedStatement ps=null;
			   ResultSet rs=null;
			try
			{
			con=DBConnection.getConnection();
			st=con.createStatement();
			rs=st.executeQuery("select ID from tbl_car where ID="+id);
			 if(!rs.next()) //Checking whether the row is present or not
			 {
				System.out.println("The given ID is not present in the table");
			}
			else {
				System.out.println("Select the column:\n1.Make\n2.Model\n3.Year\n4.Sales_Price:");
    			System.out.println("Enter the column name:");
    			String column=sc.nextLine();
    			System.out.println("Enter new value:");
    			String value=sc.nextLine();
			ps=con.prepareStatement("update tbl_car set "+column+"=? where ID=?");  // query to insert the updated value of column based on id value
			ps.setString(1, value);
			ps.setInt(2,id);
			ps.executeUpdate();  // Executing the above update query
			System.out.println("Updated the row with given"+column);
		}
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
			 finally
			   {
				   con.close();
				   rs.close();
				   st.close(); 
			   }
		}
			public void deleteMethod(int id) throws Exception{
				   Connection con=null;
				   Statement st=null;
				   PreparedStatement ps=null;
				   ResultSet rs=null;
				try
				{
		    	con=DBConnection.getConnection();
		    	st=con.createStatement();
		    	rs=st.executeQuery("select ID from tbl_car where ID="+id);
				if(!rs.next()) //Checking whether the row is present or not
				{
					System.out.println("The given ID is not present in the table");
				}
				else {
				ps=con.prepareStatement("delete from tbl_car where ID=?"); // To delete the row from the table based upon id
				ps.setInt(1, id);
				ps.executeUpdate();  // Executing the above delete query
				System.out.println("Deleted the row with ID:"+id);
					
				}
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
				 finally
				   {
					   con.close();
					   rs.close();
					   st.close();	   
				   }
			}
   }


