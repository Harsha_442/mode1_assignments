package com.carinventory.service;

import com.carinventory.dao.CarInventoryCrudOperations;
import com.carinventory.pojo.CarInventoryPojo;

public class CarInventoryService {
   CarInventoryCrudOperations cco=new CarInventoryCrudOperations();
   public void addCar(CarInventoryPojo pojo) throws Exception
   {
	   cco.addMethod(pojo);
   }
   public void listCar() throws Exception
   {
	   cco.listMethod();
   }
   public void updateCar( int id) throws Exception
   {
	   cco.updateMethod(id);
   }
   public void deleteCar(int id) throws Exception
   {
	   cco.deleteMethod(id);
   }
   }
