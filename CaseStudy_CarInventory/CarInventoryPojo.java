package com.carinventory.pojo;

public class CarInventoryPojo {
  private int id;
  private String make;
  private String model;
  private int year;
  private float sales_price;
   
public CarInventoryPojo(int id,String make,String model,int year,float sales_price)
  {
	  super();
	  this.id=id;
	  this.make=make;
	  this.model=model;
	  this.year=year;
	  this.sales_price=sales_price;
  }
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getMake() {
	return make;
}
public void setMake(String make) {
	this.make = make;
}
public String getModel() {
	return model;
}
public void setModel(String model) {
	this.model = model;
}
public int getYear() {
	return year;
}
public void setYear(int year) {
	this.year = year;
}
public float getSales_price() {
	return sales_price;
}
public void setSales_price(float sales_price) {
	this.sales_price = sales_price;
}
  
}
