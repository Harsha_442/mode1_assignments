package com.carinventory.main;

import java.util.Scanner;

import com.carinventory.pojo.CarInventoryPojo;
import com.carinventory.service.CarInventoryService;

public class Main {
   public static void main(String[] args) {
	   Scanner sc=new Scanner(System.in);
	   CarInventoryService carInventoryService=new CarInventoryService();
	    String command=""; 
	    System.out.println("Welcome to the car inventory system!!!");    
	    try
	    {
	    	do// For repeating the loop until the user gives quit command
	    	{
	    		System.out.print("1.Add\n2.List\n3.Update\n4.Delete\n5.Quit\nEnter Command:\n");  
	    		command=sc.nextLine();
	    		
	    		switch(command)// going to particular case depending upon the input which is given by user
	    		{
	    		case "add": // Add operation - To add the data
	    			System.out.println("ID:");
	    		    int id=Integer.parseInt(sc.nextLine());
	    			System.out.println("Make:");
	    			String make=sc.nextLine();
	    			System.out.println("Model:");
	    			String model=sc.nextLine();
	    			System.out.println("Year:");
	    			int year=Integer.parseInt(sc.nextLine());
	    			System.out.println("Sales Price ($):");
	    			float sales_price=Float.parseFloat(sc.nextLine());
	    			CarInventoryPojo inputs=new CarInventoryPojo(id,make,model,year,sales_price);
	    			carInventoryService.addCar(inputs);
	    			break;
	    		case "list": // List operation- for displaying the data from table
	    			carInventoryService.listCar();
	    			break;
	    		case "update": // Update operation-To update the  particular column
	    			System.out.println("Enter ID:");
	    			id=Integer.parseInt(sc.nextLine());
	    			carInventoryService.updateCar(id);
	    			break;
	    		case "delete":  // Delete operation - To delete the particular column
	    			System.out.println("Enter ID:");
	    			id=Integer.parseInt(sc.nextLine());
	    			carInventoryService.deleteCar(id);
	                
	    			break;
	    		case "quit": //Quit operation - To exit 
	    			 System.out.println("Thank you and Good bye!!");
	    	         System.exit(0); // To exit/terminate from the do-while loop
	    	         break;
	    	    default:
	    	    	System.out.println("Sorry,but "+command+" is not a valid command. Please try again!!");
	    		}
	    		
	    	} while(command!="quit");
	    	
	    }
	    catch(Exception e) 
	    {
	    	System.out.println(e);
	    }
	    finally
	    {
	    	sc.close();
	    }
    }
}
