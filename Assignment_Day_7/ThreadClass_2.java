/*Rewrite the earlier program so that, now the class DemoThread1 instead of implementing 
 * from Runnable interface, will now extend from Thread class. 
 */
package Assignment_Day_7;

class SubThread2 extends Thread{
public SubThread2() {
	Thread t1=new Thread();
	t1.start();
	
}
public void run() {
	for(int i=1;i<=10;i++) {
		System.out.println("running child Thread in loop : " + i);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}

}
public class ThreadClass_2 {
	public static void main(String[] args) {
		System.out.println(1);
		SubThread2 th1=new SubThread2();
		SubThread2 th2=new SubThread2();
		SubThread2 th3=new SubThread2();
		th1.start();
		th2.start();
		th3.start();
	}

}
