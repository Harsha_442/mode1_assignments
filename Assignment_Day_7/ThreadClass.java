/*Write a program to create a class DemoThread1 implementing Runnable interface.
 *  In the constructor, create a new thread and start the thread. 
 *  In run() display a message "running child Thread in loop : " display the value
 *   of the counter ranging from 1 to 10. Within the loop put the thread to sleep for 2 seconds.
 *    In main create 3 objects of the DemoTread1 and execute the program. 
 */
package Assignment_Day_7;

  class ThreadClass1 implements Runnable{
	  void ThreadClass1(){
			Thread th1=new Thread();
			th1.start();
		}			
	public void run() {
		for(int i=1;i<=10;i++) {
			System.out.println("Running child Thread in loop:"+i);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}				
		}	
	}
  }
	
public class ThreadClass{
	public static void main(String[] args) {
		System.out.println(1);
		ThreadClass1 t1=new ThreadClass1();
		ThreadClass1 t2=new ThreadClass1();
		ThreadClass1 t3=new ThreadClass1();
		Thread th1=new Thread();
		Thread th2=new Thread();
		Thread th3=new Thread();
		th1.start();
		th2.start();
		th3.start();		
	}
		}
	 


