/*Write a program to assign the current thread to t1. Change the name of the thread to MyThread.
 *  Display the changed name of the thread. Also it should display the current time.
 *   Put the thread to sleep for 10 seconds and display the time again. 
 */
package Assignment_Day_7;
import java.time.LocalTime;

class Sub_Thread extends Thread{
	public void run() {
		System.out.println(java.time.LocalTime.now());
		try {
			Sub_Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(java.time.LocalTime.now());
	}
}

public class Thread1 {
	public static void main(String[] args) {
		Sub_Thread t1= new Sub_Thread();
		System.out.println("Name of t1:"+t1.getName());
		t1.start();
		t1.setName("MyThread");
		System.out.println("After changing the name of thread:"+t1.getName());
	}

}
