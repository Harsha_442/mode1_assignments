/*In the previous program remove the try{}catch(){} block surrounding the sleep method
 *  and try to execute the code. What is your observation?
 */

package Assignment_Day_7;
import java.time.LocalTime;
class Sub_Thread2 extends Thread{
		public void run() {
			LocalTime time=LocalTime.now();
			System.out.println(time);
			//try {
				Thread.sleep(1000);
			//} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			//}
			System.out.println(time);
		}
	}

	public class Thread2 {
		public static void main(String[] args) {
			Sub_Thread t1= new Sub_Thread();
			System.out.println("Name of t1:"+t1.getName());
			t1.start();
			t1.setName("MyThread");
			System.out.println("After changing the name of thread:"+t1.getName());
		}

	}

