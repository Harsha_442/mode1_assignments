/*Write a program to create a class Number  which implements Runnable. Run method displays
 *  the multiples of a number accepted as a parameter. In main create three objects - first
 *  object should display the multiples of 2, second should display the multiples of 5 and 
 *  third should display the multiples of 8. Display appropriate message at the beginning and
 *  ending of thread. The main thread should wait for the first object to complete.
 *  Display the status of threads before the multiples are displayed and after completing
 *  the multiples. 
 */
package Assignment_Day_7;

	public class ThreadMul   implements Runnable{
		 ThreadMul (int input){
	        display(input);
	    }
	public static synchronized void display(int input) {
	    System.out.println("Start displaying the "+input+" multiples:");
	    for(int i=1;i<=10;i++) {
	        
	            System.out.println("multiples of "+input+" are :"+i*input);
	        
	    }
	    System.out.println("Stop displaying the "+input+" multiples:");
	}
	    public static void main(String[] args) throws InterruptedException {
	    	 ThreadMul  thread1=new  ThreadMul(2);
	    	 ThreadMul  thread2=new  ThreadMul(5);
	    	 ThreadMul  thread3=new  ThreadMul(8);
	        Thread t1=new Thread(thread1);
	        t1.start();
	        //t1.join();
	        Thread t2=new Thread(thread2);
	        t2.start();
	        //t2.join();
	        Thread t3=new Thread(thread3);
	        t3.start();
	        //t3.join();
	        
	        
	    }

	 

	    @Override
	    public void run() {
	        // TODO Auto-generated method stub
	        
	    }

	 

	}
