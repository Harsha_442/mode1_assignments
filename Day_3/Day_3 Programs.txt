1.
//To convert all the characters in a string to lowercase
import java.util.Scanner;
	
	public class Lowercase {
		public static void main(String [] args) {
			
		Scanner sc= new Scanner(System.in);
		String s;
		int i;
	    System.out.println("Enter the string:");
	    s=sc.nextLine();
	    char str[]=s.toCharArray();
	    for(i=0;i<s.length();i++) {
	    	if(str[i]>='A' && str[i]<='Z') {
	    		str[i]=(char)((int)str[i]+32);
	    	}
	 
	    }
	    System.out.println("The string in lower case is:");
	    for(i=0;i<str.length;i++) {
	    	System.out.print(str[i]);
	    }
	    sc.close();
		}	
	}

2.
//To replace all the 'd' occurrence characters with ‘h’ characters in each string
import java.util.Scanner;

public class Replace {
  public static void main(String[]args) {
	  Scanner sc= new Scanner(System.in);
	  System.out.println("Enter the string:");
	  String s;
	  s=sc.nextLine();
	  char str[]=s.toCharArray();
	  for(int i=0;i<s.length();i++) {
		  if(str[i]=='d') {
			  str[i]='h';
		  }
		  }
	  System.out.println("The string after replacing is:");
		  for(int i=0;i<str.length;i++) {
			  System.out.print(str[i]);
		  
	  }
	  
	sc.close();  
  }
}

3.
//To sort an integer array of 10 elements in ascending
import java.util.Scanner;

public class Sortinteger {
	public static void main(String[]args) {
		Scanner sc=new Scanner(System.in);
		int i,j,arr[],n,m=0;
		System.out.println("Enter the count:");
		n=sc.nextInt();
	    arr=new int[n];
		System.out.println("Enter elements to an array:");
		for(i=0;i<n;i++) {
			arr[i]=sc.nextInt();
		}
		for(i=0;i<arr.length;i++)
		{
			for(j=i+1;j<arr.length;j++)
			{
			if(arr[i]>arr[j]) {
				m=arr[i];
				arr[i]=arr[j];
				arr[j]=m;
			}
			}
		}
		System.out.println("The ascending order is:");
			for(i=0;i<arr.length;i++) {
				System.out.print(arr[i]+" ");
			}
			sc.close();
		}
}

4.
//To search for an element of an integer array of 10 elements.

import java.util.Scanner;
public class Searchelement {
	public static void main(String[]args) {
		Scanner sc=new Scanner(System.in);
		int i,arr[],n,h,m=0;
		System.out.println("Enter the count:");
		n=sc.nextInt();
	    arr=new int[n];
		System.out.println("Enter elements to an array:");
		for(i=0;i<n;i++) {
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter the search element:");
		h=sc.nextInt();
		for(i=0;i<n;i++)
		{
			if(arr[i]==h) {
				m++;
				break;
			}
		}
		if(m==1)
		{
			System.out.println("Given number is present in the array");
			}
			else {
				System.out.println("Given number is not present in the array");
			}
		sc.close();
		}
}
		
5.
import java.util.Scanner;
public class Substring {
	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);
		String s;
		System.out.println("Enter the string");
		s=sc.nextLine();
		int m,n;
		System.out.println("Enter the limit numbers");
		m=sc.nextInt();
		n=sc.nextInt();
		System.out.println("The substring is:"+s.substring(m,n));	
		sc.close();		
	}
}

6.
import java.util.Scanner;
public class Palindrome {
public static void main(String[]args) {
	Scanner sc=new Scanner(System.in);
	String s,r="";
	System.out.println("Enter the string:");
	s=sc.nextLine();
	int i=0;
	int j=s.length();
	for(i=(j-1);i>=0;i--) {
		r=r+s.charAt(i);
	}
	if(s.toLowerCase().equals(r.toLowerCase())) {
		System.out.println("Yes");
	}
	else {
		System.out.println("No");
	}
	sc.close();
	}	
}
7.
import java.util.Scanner;

public class Panagram {
	 public static void main(String[] args) {
		 Scanner sc=new Scanner (System.in);
		 System.out.print("Enter the string:");
		 String str = sc.nextLine();
	      boolean[] alphaList = new boolean[26];
	      int index = 0;
	      int temp = 1;
	      for (int i = 0; i < str.length(); i++) {
	         if ( str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') {
	            index = str.charAt(i) - 'A';
	         }else if( str.charAt(i) >= 'a' && str.charAt(i) <= 'z') {
	            index = str.charAt(i) - 'a';
	      }
	      alphaList[index] = true;
	   }
	   for (int i = 0; i <= 25; i++) {
	      if (alphaList[i] == false)
	      temp = 0;
	   }
	   if (temp == 1)
	      System.out.print(" Pangram");
	   else
	      System.out.print("Not a pangram");
	   sc.close();
	   }
	}

8.
import java.util.Scanner;

public class Modify {
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	String s;
	System.out.println("Enter the string");
	 s=sc.nextLine();
	 System.out.println(New.getResult(s));
	sc.close();
}
}
 class New{
  public static String getResult(String s) {
	  if(s.charAt(0)=='k'|| s.charAt(0)=='K') {
		  String str=s.charAt(0)+s.substring(2);
		  return str;
	  }
		  else if(s.charAt(1)=='b'|| s.charAt(1)=='B') {
			  String str=s.charAt(1)+s.substring(2);
			  return str;
		  }
		  else {
			  String str=s.substring(2);	
			  return str;
	  }
	
  }
 }
 
9.
import java.util.Scanner;
public class Main {
 public static void main(String[] args) {
	 Scanner sc= new Scanner(System.in);
	 System.out.println("Enter the string");
	 String s=sc.nextLine();
	 System.out.println("Enter the character");
	 char c=sc.next().charAt(0);
	 System.out.println(UserMainCode.reShape(s,c));	
	 sc.close();
 }
} 
class UserMainCode{
	public static String reShape(String s ,char c) {
		String r="";
		for(int i=s.length()-1;i>=0;i--) {
			if(i==0)
			{
				r=r+s.charAt(i);
			}
			else {
			r=r+s.charAt(i)+c;	
			}
		}
		return r;	
	}
}

10.
//How to use add method from another package.
package com.hcl;

public class Calculator {
	public int add(int a, int b){
		return a+b;
		}
}


import com.hcl.*;
import java.util.Scanner;
public class Newpackage {
	public static void main(String[]args) {
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the values for a and b");
	int a=sc.nextInt();
	int b=sc.nextInt();
	Calculator c=new Calculator();
	System.out.println(c.add(a,b));
	sc.close();
	
}
}
