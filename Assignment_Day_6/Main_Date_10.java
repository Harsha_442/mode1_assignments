/*Write a program to read  two String variables in DD-MM-YYYY.Compare the two dates and return 
 * the older date in 'MM/DD/YYYY' format.Include a class UserMainCode with a static method
 *  findOldDate which accepts the string values. The return type is the string.Create a Class
 *   Main which would be used to accept the two string values and call the static method
 *    present in UserMainCode. 
Sample Input 1: 
05-12-1987 
8-11-2010 
Sample Output 1: 
12/05/1987 
*/

package Assignment_Day_6;
import java.util.Scanner;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

class User_Main_Code{
	public static String findOldDate(String date1,String date2) throws ParseException {
		String date3="";
		Date d1=new SimpleDateFormat("DD-MM-YYYY").parse(date1);
		Date d2=new SimpleDateFormat("DD-MM-YYYY").parse(date1);
		int difference=d1.compareTo(d2);
		if(difference>0) {
			date3=new SimpleDateFormat("MM/DD/YYYY").format(d2);
		}
		else if (difference<0) {
		 
		 date3=new SimpleDateFormat("MM/DD/YYYY").format(d1);
		}
		else  {
			date3="Both dates are equal";
		}
		return date3;		
	}
}
public class Main_Date_10 {
	public static void main(String[] args) throws ParseException {
	Scanner sc=new Scanner(System.in);
	SimpleDateFormat sdf=new SimpleDateFormat("DD-MM-YYYY");
	System.out.println("Enter the first date:");
	String date1=sc.nextLine();
	System.out.println("Enter the second date:");
	String date2=sc.nextLine();
	String d3 = " ";
	d3=User_Main_Code.findOldDate(date1, date2);
	 System.out.println("Old Date:");
	System.out.println(d3);
	sc.close();
}
}
