/*Write a program which would accept a string and a character as a delimiter. Apply the below
 *  rules
1. Using the delimiter, split the string and store these elements in array.
2. Reverse each element of the string and convert it into lowercase.
Include a class UserMainCode with a static method manipulateLiteral which accepts the string 
and character. The return type is the string array formed.
Create a Class Main which would be used to accept the string and character and call the static
 method present in UserMainCode.
Input and Output Format:

Input consists of a string and character.
Output consists of a string array.

Refer sample output for formatting specifications.

Sample Input 1:
AAA/bba/ccc/DDD
/

Sample Output 1:
aaa
abb
ccc
ddd
*/
package Assignment_Day_6;
	import java.util.Scanner;

	class StringSplitterDemo{
		static String[] manipulateLiteral(String str,char delimiter) {
			String[] sortedArray=str.split("\\" + delimiter + "");
			for(int i=0;i<sortedArray.length;i++) {
				char[] ch=sortedArray[i].toCharArray();
				sortedArray[i]="";
				for(int j=ch.length-1;j>=0;j--) {
					sortedArray[i]=sortedArray[i]+ch[j];
				}
				sortedArray[i]=sortedArray[i].toLowerCase();
			}
			return sortedArray;	
		}
	}
	public class StringSplitter_18 {
		public static void main(String[] args) {
			Scanner sc=new Scanner(System.in);
			System.out.println("Enter the String: ");
			String st=sc.next();
			System.out.println("Enter the delimiter");
			char del=sc.next().charAt(0);
			String[] sorted=StringSplitterDemo.manipulateLiteral(st,del);
			for(int i=0;i<sorted.length;i++) {
				System.out.println(sorted[i]);
			}
			sc.close();
		}
	}


