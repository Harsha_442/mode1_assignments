//Write a java program to print current date and time in the specified format.

package Assignment_Day_6;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CurrentDate_1 {
	public static void main(String[] args) {
	SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	 Date date=new Date();               
    System.out.println(sdf.format(date));
	} 
}
