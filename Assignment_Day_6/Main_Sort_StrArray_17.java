/*Write a program to read a string array, remove duplicate elements and sort the array.
Note:
1.	    The check for duplicate elements must be case-sensitive. (AA and aa are NOT duplicates)
2.	    While sorting, words starting with upper case letters takes precedence.

Include a class UserMainCode with a static method orderElements which accepts the string array.
 The return type is the sorted array.

Create a Class Main which would be used to accept the string arrayand integer and call the
 static method present in UserMainCode.

Input and Output Format:

Input consists of an integer n which is the number of elements followed by n string values.

Output consists of the elements of string array.

Refer sample output for formatting specifications.

Sample Input 1:
6
AAA
BBB
AAA
AAA
CCC
CCC

Sample Output 1:
AAA
BBB
CCC


*/
package Assignment_Day_6;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class Sub_Sort_StrArray{
	static String[] orderElements(String[] str) {
		for(int i=0;i<str.length;i++) {
			if(str[i]!=null) {
				for(int j=i+1;j<str.length;j++) {
					if(str[i].equals(str[j])) {
						str[j]="";
					}
				}
			}	
		}
		List<String> lst=new ArrayList<String>();
		for(int i=0;i<str.length;i++) {
			if(!(str[i].isEmpty())) {
				lst.add(str[i]);
			}
		}
		Collections.sort(lst);
		
		String[] str1=new String[lst.size()];
		for(int i=0;i<str1.length;i++) {
			str1[i]=lst.get(i);
		}
		return str1;
		
	}
}

public class Main_Sort_StrArray_17 {
 public static void main(String[] args) {
			Scanner sc=new Scanner(System.in);
			System.out.println("Enter the size of array: ");
			int size=sc.nextInt();
			System.out.println("Enter the String: ");
			String[] inputStr=new String[size];
			for(int i=0;i<size;i++) {
				inputStr[i]=sc.next();
			}
			String[] str= Sub_Sort_StrArray.orderElements(inputStr);
			for(int i=0;i<str.length;i++) {
				System.out.println(str[i]);
			}
			sc.close();
		}
 }
