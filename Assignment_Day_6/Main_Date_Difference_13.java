/*Get two date strings as input and write code to find difference between two dates in days. 
Include a class UserMainCode with a static method getDateDifference which accepts two date 
strings as input.The return type of the output is an integer which returns the difference
 between two dates in days.Create a class Main which would get the input and call the
  static method getDateDifference present in the UserMainCode. 
  
Sample Input 1: 
2012-03-12 
2012-03-14 
Sample Output 1: 
2 
 
Sample Input 2: 
2012-04-25 
2012-04-28 
Sample Output 2: 
3 
 
*/

package Assignment_Day_6;
import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;

class Sub_Date_Difference{
	 static int getDateDifference(String date1,String date2) {
		 Period diff=Period.between(LocalDate.parse(date1), LocalDate.parse(date2));
         return diff.getDays();
		 
	 }
}
public class Main_Date_Difference_13 {

	public static void main(String[]args) {
		Scanner sc=new Scanner(System.in);
        System.out.println("Enter the date 1: ");
        String date1=sc.next();
        System.out.println("Enter the date 2: ");
        String date2=sc.next();
        System.out.println(Sub_Date_Difference.getDateDifference(date1,date2));
        sc.close();
	}
}
