
/*
import java.util.Calendar;
/*Given two inputs year and month (Month is coded as: Jan=0, Feb=1 ,Mar=2 ...), 
 write a program to find out total number of days in the given month for the given year. 
 Include a class UserMainCode with a static method �getNumberOfDays� that accepts 2 integers
 as arguments and returns an integer. The first argument corresponds to the year and the
 second argument corresponds to the month code. The method returns an integer corresponding
 to the number of days in the month. 
 Create a class Main which would get 2 integers as input and call the static method getNumberOfDays 
 present in the UserMainCode. 
 Input and Output Format: 
 Input consists of 2 integers that correspond to the year and month code. 
 Output consists of an integer that correspond to the number of days in the month in the
 given year. 
Sample Input: 
2000 
1 
Sample Output: 
29 
*/
package Assignment_Day_6;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;
class User_Main {
	public static int getNumberOfDays(int year,int month) {
		Calendar cal=new GregorianCalendar(year,month,1);
		int days=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		return days;
	}
}
public class Main_6 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Year: ");
		int year=sc.nextInt();
		System.out.println("Enter the Month: ");
		int month=sc.nextInt();
		System.out.println("Number of days in a given month: ");
		System.out.println(User_Main.getNumberOfDays(year,month));
		sc.close();
	}
	}

