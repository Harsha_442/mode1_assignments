/*Write a program to read a string of even length and to fetch two middle most characters
 * from the input string and return it as string output.Include a class UserMainCode with a
 * static method getMiddleChars which accepts a string of even length as input . The return
 * type is a string which should be the middle characters of the string.Create a class Main
 * which would get the input as a string and call the static method getMiddleCharspresent 
 * in the UserMainCode.
 
Input and Output Format:
Input consists of a string of even length.
Output is a string .
Refer sample output for formatting specifications.
 
Sample Input 1:
this
Sample Output 1:
hi
 
Sample Input 1:
Hell
Sample Output 1:
el

*/
package Assignment_Day_6;
import java.util.Scanner;
class Sub_Middle_Char{
	public static String getMiddleChars(String s) {
		String s1="";
		int n1=s.length(),mid=0;
		mid=n1/2;
		String s2;
		s2=s.substring(mid-1,mid+1);
		return s2;
	}
}

public class Main_Middle_Char_16 {
  public static void main(String[] args) {
	  Scanner sc=new Scanner(System.in);
	  System.out.println("Enter the string");
	  String s=sc.next();
	  int n=s.length();
	  if(n%2!=0) {
		  System.out.println("Invalid length of String");
	  }
	  String str="";
	  str=Sub_Middle_Char.getMiddleChars(s);
	  System.out.println(str);
	  
  }
}
