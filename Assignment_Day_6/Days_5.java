/*Given two inputs year and month (Month is coded as: Jan=0, Feb=1 ,Mar=2 ...),
 * write a program to find out total number of days in the given month for the given year. 
 */

package Assignment_Day_6;
import java.util.Calendar;
import java.util.Scanner;
public class Days_5 {
	public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	int year,month;
	System.out.println("Enter the year:");
	year=sc.nextInt();
	System.out.println("Enter the month:");
	month=sc.nextInt();
	System.out.print(display(year,month));
	}
	public static int display(int year,int month) {
		Calendar cal=Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		int DAY_OF_MONTH= cal.getActualMaximum(cal.DAY_OF_MONTH);
		return DAY_OF_MONTH;

	}
		
}
