
/*Write a code to read two int array lists of size 5 each as input and to merge the two
arrayLists, sort the merged arraylist in ascending order and fetch the elements at 2nd,
6th and 8th index into a new arrayList and return the final ArrayList. 
Include a class UserMainCode with a static method sortMergedArrayList which accepts 2 ArrayLists. 
The return type is an ArrayList with elements from 2,6 and 8th index position .
Array index starts from position 0. 
Create a Main class which gets two array list of size 5 as input and call the static method sortMergedArrayList present in the UserMainCode. 
Sample Input: 
3 
1 
17 
11 
19 
5 
2 
7 
6 
20 
Sample Output: 
3 
11 
19 
*/
package Assignment_Day_6;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

class Usermaincode{
	public static ArrayList<Integer> SortedMergedList(ArrayList<Integer> l1, ArrayList <Integer> l2) {
		ArrayList<Integer> l3=new ArrayList(10);
		l1.addAll(l2);
		Collections.sort(l1);
		l3.add(l1.get(2));
		l3.add(l1.get(6));
		l3.add(l1.get(8));
		return l3;
		
	}
}

public class IndexElement_8 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		ArrayList<Integer> l1=new ArrayList(5);
		ArrayList<Integer> l2=new ArrayList(5);
		ArrayList<Integer> l3=new ArrayList(5);
		System.out.println("Enter the elements for List-1:");
		for(int i=0;i<5;i++) {
		l1.add(sc.nextInt());
		}
		System.out.println("Enter the elements for List-2:");
		for(int i=0;i<5;i++) {
			l2.add(sc.nextInt());
			}
		l3=Usermaincode.SortedMergedList(l1,l2);
		System.out.println("Output:");
		for(int k=0;k<3;k++) {
			System.out.println(l3.get(k));
		}
	}

}
