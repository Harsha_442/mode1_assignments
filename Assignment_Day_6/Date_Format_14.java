/* Given a date string in the format dd/mm/yyyy, write a program to convert the given date to the
 * format dd-mm-yy. Include a class UserMainCode with a static method “convertDateFormat”
 * that accepts a String and returns a String. Create a class Main which would get a 
 * String as input and call the static method convertDateFormat present in the UserMainCode. 
Sample Input: 
12/11/1998 
  
Sample Output: 
12-11-98 
*/


package Assignment_Day_6;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
class Sub_DateFormat{
	public static String convertDateFormat(String date1) throws ParseException {
		String date2="";
		Date d1=new SimpleDateFormat("dd/mm/yyyy").parse(date1);
		date2=new SimpleDateFormat("DD-MM-YYYY").format(d1);
		return date2;		
	}	
	}

public class Date_Format_14 {
	public static void main(String[] args) throws ParseException {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the  date:");
		String date1=sc.next();
		System.out.println("Converted Date:");
		System.out.println(Sub_DateFormat.convertDateFormat(date1));
		sc.close();
		
	}

}
