
/*Write a program to read a string  and to test whether first and last character are same.
 *  The string is said to be be valid if the 1st and last character are the same. 
 *  Else the string is said to be invalid. 
Include a class UserMainCode with a static method checkCharacters which accepts a string as input . 
The return type of this method is an int.  Output should be 1 if the first character and
 last character are same . If they are different then return -1 as output. 
Create a class Main which would get the input as a string and call the static method 
checkCharacters present in the UserMainCode. 
  
Sample Input : 
the picture was great 
Sample Output : 
Valid 
  
Sample Input : 
this 
Sample Output : 
Invalid 
*/
package Assignment_Day_6;
import java.util.Scanner;
class UserMain{
	public static int checkCharacters(String s) {
		
		if(s.charAt(0)==s.charAt(s.length()-1)) {
			return 1;
		}
		else {
			return -1;
		}		
	}	
}
public class MainClass_7 {
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the string:");
	String s=sc.nextLine();
	UserMain rep=new UserMain();
	int r=rep.checkCharacters(s);
	if(r==1) {
		System.out.println("Valid");
	}
	else {
		System.out.println("Invalid");
	}
}
}
