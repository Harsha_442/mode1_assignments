/*Write a program to read a string and validate the IP address. Print “Valid” if the
 *  IP address is valid, else print “Invalid”.Include a class UserMainCode with a static method
 *   ipValidator which accepts a string. The return type (integer) should return 1 if it is a
 *   valid IP address else return 2. Create a Class Main which would be used to accept Input 
 *   String and call the static method present in UserMainCode. 
 
Note: An IP address has the format a.b.c.d where a,b,c,d are numbers between 0-255. 
  
Sample Input 1: 
132.145.184.210 
Sample Output 1: 
Valid 
  
Sample Input 2: 
132.145.184.290 
Sample Output 2: 
Invalid 
*/
package Assignment_Day_6;
import java.util.Scanner;
class SubIP{
	public static int ipValidator(String ip_Address){
		String[] arr=new String[4];
		int i;
		arr=ip_Address.split("\\.");
		for(i=0;i<arr.length;i++) {
			int temp=Integer.parseInt(arr[i]);
			if(!(temp>=0&&temp<=255)) {
				break;
			}
		}
		if(i==arr.length) {
			return 1;
		}else {
			return 2;
		}
		
	}
}
public class IP_12 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the IP Address: ");
		String ip_Address=sc.next();
		int result=SubIP.ipValidator(ip_Address);
		if(result==1) {
			System.out.println("Valid");
		}else {
			System.out.println("Invalid");
		}
		sc.close();
	
	

}
}
