
//Write a Java program to extract date, time from the date string.
package Assignment_Day_6;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Scanner;
public class ExtractDate_2 {
	public static void main(String[] args) throws Exception {
		try {
			String dateString;
			Scanner sc=new  Scanner(System.in);
			System.out.println("Enter the date string: ");
			dateString=sc.nextLine();
			Date d=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(dateString);
			String date=new SimpleDateFormat("dd/MM/yyyy").format(d);
			String time=new SimpleDateFormat("HH:mm:ss").format(d);
			System.out.println("Date is: " + date);
			System.out.println("Time is: " + time);
			sc.close();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
