
/*A Company wants to obtain employees of a particular designation. You have been assigned as 
 the programmer to build this package. You would like to showcase your skills by creating 
 a quick prototype. The prototype consists of the following steps: 
 Read Employee details from the User. The details would include name and designaton in the
 given order. The datatype for name and designation is string. 
 Build a hashmap which contains the name as key and designation as value. 
 You decide to write a function obtainDesignation which takes the hashmap and designation
 as input and returns a string array of employee names who belong to that designation as
 output. Include this function in class UserMainCode. 
 Create a Class Main which would be used to read employee details in step 1 and
 build the hashmap. Call the static method present in UserMainCode. 
Sample Input 1: 
4 
Manish 
MGR 
Babu 
CLK 
Rohit 
MGR 
Viru 
PGR 
MGR 
 
Sample Output 1: 
Manish 
Rohit 
*/
package Assignment_Day_6;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Scanner;

class MainCode{
	public static String[] obtainDesignation(LinkedHashMap<String,String>map,String a)
	{
		 String [] arr1=new String[10];
		 int i=0;
		 Iterator<String> it=map.keySet().iterator();
		 while(it.hasNext())
		 {
			 String s2=it.next();
			 String s3=map.get(s2);
			 if(s3.equals(a))
			 {
				arr1[i]= s2;
				i++;
			 }			 
		 }
		return arr1 ;
	}
}
public class MainClass1_9 {
   public static void main(String [] args) {
	   Scanner sc= new Scanner(System.in);
	   System.out.println("Enter the count of key and values for Hash Map:");
	   int n=sc.nextInt();
	   LinkedHashMap<String,String>map=new LinkedHashMap<String,String>();
	   String []arr2=new String[10];
	   System.out.println("Enter the key and values for Hash Map:");
	   String a =" ",b="";
	   for(int i=0;i<n;i++) {
		    a=sc.next();
		    b=sc.next();
		  map.put(a, b);
	   }
	   System.out.println("Enter the value to be searched:");
	      String n1=sc.next();
		  arr2=MainCode.obtainDesignation(map,n1);
		for(int i=0;i<arr2.length;i++) {
			System.out.println(arr2[i]);
		}
	   }
   }

