/*Write a Java program to get and display information (year, month, day, hour, minute)
of a default calendar */

package Assignment_Day_6;
import java.util.Calendar; 
	public class Calculator_3 {
	    public static void main(String[] args) {
	        Calendar cal=Calendar.getInstance();
	        System.out.println("Year:" + cal.get(Calendar.YEAR));
	        System.out.println("Month :" + cal.get(Calendar.MONTH));
	        System.out.println("Day :" + cal.get(Calendar.DATE));
	        System.out.println("Hour :" + cal.get(Calendar.HOUR));
	        System.out.println("Minute :" + cal.get(Calendar.MINUTE));
	        
	    }
	}


