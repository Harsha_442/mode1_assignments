/*Given a method with two date strings in yyyy-mm-dd format as input. Write code to find the
 *  difference between two dates in months.Include a class UserMainCode with a static method 
 *  getMonthDifference which accepts two date strings as input.The return type of the output is
 *   an integer which returns the difference between two dates in months. Create a class Main 
 *   which would get the input and call the static method getMonthDifference present in the
 *    UserMainCode. 
  
Sample Input 1: 
2012-03-01 
2012-04-16 
Sample Output 1: 
1 
Sample Input 2: 
2011-03-01 
2012-04-16 
Sample Output 2: 
13 
*/
package Assignment_Day_6;

import java.time.Period;
import java.time.LocalDate; 
import java.util.Scanner;

class Sub_Month_Diff{
	public static int  getMonthDifference(String date1,String date2) {
		Period diff=Period.between(LocalDate.parse(date1), LocalDate.parse(date2));
		return diff.getMonths();
		
	}
}
public class Main_Month_Diff_11 {
	public static void main(String[] args)  {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the first date:");
		String date1=sc.next();
		System.out.println("Enter the second date:");
		String date2=sc.next();
		 System.out.println("Old Date:");
		System.out.println(Sub_Month_Diff.getMonthDifference(date1, date2));
		sc.close();
	}
	}
